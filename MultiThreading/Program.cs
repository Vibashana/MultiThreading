﻿using System;
using System.Threading;

public class MyThread
{

  // Thread 1
  public static void Tread1()
  {
    for (int i = 0; i < 10; i++)
    {
      Console.WriteLine("Thread1 {0}", i);
    }
  }

  // Thread 2
  public static void Tread2()
  {
    for (int i = 0; i < 10; i++)
    {
      Console.WriteLine("Thread2 {0}", i);
    }
  }

}

public class MyClass
{

  public static void Main()
  {
    Console.WriteLine("Before Start Thread");

    Thread tId1 = new Thread(new ThreadStart(MyThread.Tread1));
    Thread tId2 = new Thread(new ThreadStart(MyThread.Tread2));

    tId1.Start();
    tId2.Start();

    Console.ReadLine();

  }

}

